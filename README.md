SpiroInput

Demo project to simply handle the Spiro device.

SampleScene contains the different options. Only one can be enable at a time (otherwise, exceptions will be raised).
As an example, flow values are visible in the inspector as public variable (please, use it in a better way in your future projects!).

Scripts folder organisation:
* ExpFlowController.cs: root class for breathing device input. Has to be derived to implement device that returns more than a flow value.
* Connectivity: Contains one script by connectivity option (Serial via USB, bluetooth, etc). These are not game objects.
  * Not all scripts are implemented yet.
* ParamExtractor: Allow to use an ExpFlowController and offers new features than flow value (example: max breath, breath state, expiration duration, baseline initialisation)
* SettingsExample: Facilitate the settings used for the connectivity (auto find port number, several device instanciation, etc.).
