﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerialSpiroController : ExpFlowController
{
    public string portName;
    public float flow;

    public override float FlowOut{
        get{return flow;}
    }

    protected SerialSpiro serialSpiro=null;
    public SerialSpiro SerialSpiro{set{
            if(serialSpiro!=null && serialSpiro != value){
                serialSpiro.ClosePort();
            }
            serialSpiro=value;
        }
    }

    void Start()
    {
        if (serialSpiro == null){
            serialSpiro = new SerialSpiro(portName);
        }
    }
    
    void Update()
    {
        flow = serialSpiro.FlowOut;
    }

    void OnDisable()
    {
        serialSpiro.ClosePort();
    }
}
