﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.ComponentModel;

public class ControllerAutoSearchMulti : MonoBehaviour
{
    public SerialPort port { get; protected set; }
    public SerialSpiroController model;

    void Start()
    {
        string[] names= SerialPort.GetPortNames();
        List<string> listNames=new List<string>();
        List<SerialSpiro> listSpiro=new List<SerialSpiro>();
        int controllerNumber=0;
        foreach(string name in names){

            Debug.Log(name);
            try{
                SerialSpiro serialSpiro =  new SerialSpiro(name);
                Debug.Log(name+" is a Spiro. Connected!");
                SerialSpiroController spiroController=Instantiate(model);                
                controllerNumber++;
                spiroController.transform.parent=this.transform;
                spiroController.transform.name = "Controller"+controllerNumber;
                spiroController.portName=name;
                spiroController.SerialSpiro=serialSpiro;    
            }
            catch(System.Exception){}
        }
        //serialSpiro = new SerialSpiro(portNames[0]);
    }
}
