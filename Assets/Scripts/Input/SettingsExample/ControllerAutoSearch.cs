﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.ComponentModel;

public class ControllerAutoSearch : MonoBehaviour
{
    public SerialSpiroController model;

    void Start()
    {
        string[] portNames= SerialPort.GetPortNames();
        foreach(string name in portNames){

            Debug.Log(name);
            try{
                SerialSpiro serialSpiro =  new SerialSpiro(name);
                Debug.Log(name+" is a Spiro. Connected!");
                SerialSpiroController spiroController=Instantiate(model);
                spiroController.transform.parent=this.transform;
                spiroController.transform.name = "Controller";
                spiroController.portName=name;
                spiroController.SerialSpiro=serialSpiro;   
                break;
            }
            catch(System.Exception e){
                Debug.Log(name+" is not a Spiro.");
            }
        }
    }
}
