﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class SerialSpiro
{
    [SerializeField]
    public string PortName { get; set; }

    [SerializeField]
    protected int BaudRate { get; set; }
    [SerializeField]
    protected int serialTimeout;

    public SerialPort Port { get; protected set; }

    private float StrongBreathValue { get; set; }
    private float CalibrationFactor { get; set; }

    public bool HasBeenInitialized{get{return hasBeenInitialized;}}
    protected bool hasBeenInitialized;

    public SerialSpiro(string portName, int baudRate=9600, int serialTimeout=100)
    {
        this.PortName = portName;
        this.BaudRate = baudRate;
        this.serialTimeout = serialTimeout; 

        this.Initialize(); 
    }

    public virtual void Initialize()
    {
        if (hasBeenInitialized)
        {
            Debug.Log("Board Already Initialized");
            return;
        }

        try
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            Port = new SerialPort(PortName, BaudRate);
#else
		port = new SerialPort(PortName, baudRate);
#endif
            Port.ReadTimeout = serialTimeout;
            Port.WriteTimeout = serialTimeout;
            Port.DtrEnable = true;
            Port.RtsEnable = true;

            Port.Open();

            float initValue=FlowOut;

            hasBeenInitialized = true;
        }
        catch (System.Exception)
        {
            throw;
            //Debug.LogException(exception);
        }
    }
    

    public float FlowOut{
        get{
            if (Port.IsOpen)
            { // stream.available() > 0; ?
                try
                {
                    // reads serial port
                    string arduinoValue = Port.ReadLine();
                    return float.Parse(arduinoValue);
                    //	this.strongBreathValue = (this.strongBreathValue - 80) / 160;
                }
                catch (System.Exception)
                {
                    throw;
                }
            }
            return -1;   

        }
    }

    ~SerialSpiro()
    {
        if (hasBeenInitialized)
        {
            ClosePort();
        }
    }

    public void ClosePort()
    {
        Port.Close();
        hasBeenInitialized = false;
    }
}
