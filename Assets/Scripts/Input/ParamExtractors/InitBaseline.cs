﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitBaseline : MonoBehaviour
{
    private ExpFlowController expFlowController;
    public float flow;
    public KeyCode[] resetKey;

    private float baseline=0;
    private bool init=false;
    // Start is called before the first frame update
    void Start()
    {
        expFlowController=this.GetComponent<ExpFlowController>();
    }

    // Update is called once per frame
    void Update()
    {
        bool isReset=true;
        foreach(KeyCode key in resetKey){
            isReset&=Input.GetKey(key);
        }
        if(!init || isReset){
            baseline=expFlowController.FlowOut;
            Debug.Log("New calibration: baseline="+baseline);
            init=true;
        }
        flow=FlowOut;
    }

    public float FlowOut{
        get{
            return expFlowController.FlowOut-baseline;
        }
    }
}
