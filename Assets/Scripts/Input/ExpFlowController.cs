﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ExpFlowController : MonoBehaviour
{
    public abstract float FlowOut { get; }
}
